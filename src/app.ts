/* ---- IMPORTS ---- */
import express from "express";
import config from "config";
import bodyParser from "body-parser";
import cookieParser from "cookie-parser";
import mongoose from "mongoose";
import { initControllers } from "./controllers";
import authentication from "./middlewares/authentication";
import hostOnlineStatusUpdater from "./util/hostOnlineStatusUpdater";
import taskExecutor from "./util/taskExecutor";
import logger from "./util/logger";


/* ---- MAIN FUNCTION ---- */
(function main(): void {
    const app: express.Application = express();

    /* ---- DATABASE ---- */
    mongoose.connect(
        `mongodb://${ config.get("db.host") }:${ config.get("db.port") }/${ config.get("db.database") }`,
        { useNewUrlParser: true }
    ).catch(failStartup);


    /* ---- MIDDLEWARE ---- */
    app.use(bodyParser.json());
    app.use(cookieParser());
    app.use("/", (req, res, next) => 
        req.path === "/login" ? 
        next() : authentication()(req, res, next)
    );
    app.use("/", express.static("./public"));


    /* ---- CONTROLLERS ---- */
    initControllers(app);
 
    
    /* ---- OTHER ROUTINE ---- */
    hostOnlineStatusUpdater();
    taskExecutor();
    

    /* ---- SERVER START ---- */
    app.listen(process.env.PORT || 3000).on("listening", function() {
        logger.info(`Server is listening on port ${ process.env.PORT || 3000 }.`)
    }).on("error", function(err: Error) {
        failStartup(err);
    });
})();


/**
 * Loggs the error, kills the app, and returns -1 on exit status.
 * @param err the application Error.
 */
function failStartup(err: Error) {
    logger.error(`Failed to load the app.`);
    logger.error(err);
    process.exit(-1);
}


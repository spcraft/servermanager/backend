import { User, getRequestUser } from "../middlewares/authentication";
import { Request, Response, Router } from "express";
import hostRepository, { Host, findHosts } from "../models/Host";
import { NOT_FOUND, BAD_REQUEST, INTERNAL_SERVER_ERROR, OK } from "http-status-codes";
import { statusMessage, internalServerErrorMessage } from "../util/messages";
import templateRepository, { USER_QUEUE_TEMPLATE_TITLE, Template } from "../models/Template";
import taskRepository, { Task, TaskStatus } from "../models/Task";
import { Types } from "mongoose";


const controller: Router = Router();
export default controller;



/**
 * Creates a new host; the host data is provided by body
 */
controller.post("/", function(req: Request, res: Response) {
    req.body["owner"] = getRequestUser(req).uid; // Setting the host's owner as this user
    hostRepository.create(req.body).then(function(newHost: Host) {
        res.json(newHost);
    }).catch(function(err: Error) {
        res.status(BAD_REQUEST).json(
            statusMessage(BAD_REQUEST, "There was an error in processing your request", err.message)
        );
    });
});


/**
 * Gets an information about the host
 */
controller.get("/:_id", function(req: Request, res: Response) {
    hostRepository.findById(req.params["_id"]).exec().then(function(host: Host) {
        if(!host) 
            res.status(NOT_FOUND).json(
                statusMessage(NOT_FOUND, "The Host you were trying to get was not found")
            );
        else res.json(host);
    }).catch(function(err: Error) {
        res.status(INTERNAL_SERVER_ERROR).json(
            internalServerErrorMessage(err)
        );
    });
});


/**
 * Gets the queue for this server from the perspective of the request user
 * The format is specified as: 
 * {
 *     templateId: string,
 *     myActiveQueueTasks: Task[],
 *     usersOnline: string[]
 * }
 */
controller.get("/:_id/queue", function(req: Request, res: Response) {
    type QueueResponseObject = {
        templateId: string, 
        myActiveQueueTasks: Task[], 
        usersOnline: string[]
    }
    const currentUser: User = getRequestUser(req);
    const hostId: string = req.params["_id"];
    const responseObject: QueueResponseObject = { 
        templateId: null, 
        myActiveQueueTasks: [],
        usersOnline: []
    };
    templateRepository.findOne({
        host: new Types.ObjectId(hostId),
        title: USER_QUEUE_TEMPLATE_TITLE 
    }).then(function(template) {
        if(!template) res.status(NOT_FOUND).json(statusMessage(
            NOT_FOUND,
            `Either the Host is not found, or the host does not have the User Queue.`,
            `Either the Host is not found, or the host does not have the User Queue.`,
        )).send();
        else responseObject.templateId = template._id;
    }).then(function() {
        return taskRepository.find({
            template: responseObject.templateId,
            status: { $in: [ TaskStatus.PENDING, TaskStatus.READY, TaskStatus.RUNNING ] }
        }).lean();
    }).then(function(tasks: (Task & { _id: string })[]) {
        const usersOnlineSet: Set<string> = new Set<string>();
        tasks.forEach(function(task) {
            if(task.status !== TaskStatus.PENDING)
                usersOnlineSet.add(task.user);
            if(task.user === currentUser.uid)
                responseObject.myActiveQueueTasks.push(task);
        });
        usersOnlineSet.forEach(function(user) {
            responseObject.usersOnline.push(user);
        });
    }).then(function() {
        res.json(responseObject);
    }).catch(() => undefined);
});


/**
 * Modifies the host
 */
controller.put("/:_id", function(req: Request, res: Response) {
    hostRepository.findById(req.params["_id"]).exec().then(function(host) {
        if(!host) {
            res.status(NOT_FOUND).json(
                statusMessage(NOT_FOUND, "The Host you were trying to update was not found")
            );
            return;
        }
        Object.assign(host, req.body);
        return host.save();
    }).then(function(host: Host) {
        res.json(
            statusMessage(
                OK,
                "The requested Host was successfully updated, and the new version was sent back", 
                host
            )
        );
    }).catch(function(err: Error) {
        res.status(BAD_REQUEST).json(
            statusMessage(
                BAD_REQUEST,
                "An error during the object modification ocurred",
                err.message
            )
        );
    });
});


/**
 * Deletes the host
 */
controller.delete("/:_id", function(req: Request, res: Response) {
    hostRepository.findById(req.params["_id"]).then(function(host) {
        if(!host)
            res.status(NOT_FOUND).json(
                statusMessage(NOT_FOUND, "The Host you were trying to delete was not found")
            );
        else
            host.remove().then(deletedHost => (
                res.json(statusMessage(OK, "The requested Host was successfully deleted", deletedHost))
            )).catch(function(err: Error) {
                res.status(INTERNAL_SERVER_ERROR).json(internalServerErrorMessage(err));
            });
    });
});
 

/**
 * Gets a list of Host ids, given some query for search
 * If there is no query passed, returns a list of all hosts
 */
controller.get("/", function(req: Request, res: Response) {
    findHosts({
        responseType: req.query["responseType"] || "id",
        owner: req.query["owner"],
        nameLike: req.query["nameLike"]
    }).then(function(hosts: Host[] | string[]) {
        res.json(hosts);
    });
});

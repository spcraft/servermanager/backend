import { getRequestUser, User } from "../middlewares/authentication";
import { Request, Response, Router } from "express";

const controller: Router = Router();
export default controller;


/**
 * Performs the token setting into user's cookie, 
 * then redirects to redirectUrl, or to the root
 */
controller.get("/", function(req: Request, res: Response) {
    res.cookie("token", req.query["token"]);
    res.redirect(req.query["redirectUrl"] || "/");
});
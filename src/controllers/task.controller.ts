import { getRequestUser, User } from "../middlewares/authentication";
import { Request, Response, Router } from "express";
import { validateTemplate } from "../models/Template";
import { NOT_FOUND, BAD_REQUEST, INTERNAL_SERVER_ERROR, OK, FORBIDDEN } from "http-status-codes";
import { statusMessage, internalServerErrorMessage } from "../util/messages";
import taskRepository, { Task, statusString, TaskStatus } from "../models/Task";
import { TaskComponent } from "../util/taskExecutor";


const controller: Router = Router();
export default controller;


/**
 * Creates a new Task
 */
controller.post("/", function(req: Request, res: Response) {
    req.body["user"] = getRequestUser(req).uid; // Setting the task's user as this user

    validateTemplate(req.body["template"]).then(function() {
        return taskRepository.create(req.body)
    }).then(function(newTask: Task) {
        res.json(newTask);
    }).catch(function(err: Error) {
        res.status(BAD_REQUEST).json(
            statusMessage(BAD_REQUEST, "There was an error in processing your request", err.message)
        );
    });
});


/**
 * Gets an information about the Task
 */
controller.get("/:_id", function(req: Request, res: Response) {
    taskRepository.findById(req.params["_id"]).lean().then(function(task) {
        if(!task) 
            res.status(NOT_FOUND).json(
                statusMessage(NOT_FOUND, "The Task you were trying to get was not found")
            );
        else {
            task.statusString = statusString(task.status);
            res.json(task);
        }
    }).catch(function(err: Error) {
        res.status(INTERNAL_SERVER_ERROR).json(
            internalServerErrorMessage(err)
        );
    });
});

/**
 * Cancels the task
 */
controller.get("/:_id/cancel", function(req: Request, res: Response) {
    taskRepository.findById(req.params["_id"]).then(function(task) {
        if(!task) 
            res.status(NOT_FOUND).json(
                statusMessage(
                    NOT_FOUND, 
                    "The Task you were trying to cancel was not found"
                )
            );
        else if(task.status === TaskStatus.PENDING)
            Object.assign(
                task, 
                { status: TaskStatus.CANCELLED }
            ).save().then(function() {
                res.json(statusMessage(
                    OK, "The task was successfully cancelled."
                ));
            });
        else if(task.status > TaskStatus.RUNNING)
            res.status(FORBIDDEN).json(
                statusMessage(
                    FORBIDDEN, 
                    "The Task you were trying to cancel was not of " + 
                    "the state `PENDING`, `READY`, or `RUNNING`"
                )
            );
        else if(!TaskComponent.componentMap[req.params["_id"]])
            res.json(
                internalServerErrorMessage(new Error(
                    "The Task, which was supposed to be cancelled, " +
                    "did not have a TaskComponent in the executor map. " +
                    "Please, report this bug."
                ))
            )
        else
            TaskComponent.componentMap[req.params["_id"]].cancel();
            res.status(OK).json(statusMessage(OK, "The task was successfully cancelled."));
    });
});

/**
 * Modifies a Task
 */
controller.put("/:_id", function(req: Request, res: Response) {
    taskRepository.findById(req.params["_id"]).exec().then(function(found) {
        if(!found) {
            res.status(NOT_FOUND).json(
                statusMessage(NOT_FOUND, "The Task you were trying to update was not found")
            );
            return;
        }
        Object.assign(found, req.body);
        return found.save();
    }).then(function(updated: Task) {
        res.json(
            statusMessage(
                OK, 
                "The requested Task was successfully updated, and the new version was sent back", 
                updated
            )
        )
    }).catch(function(err: Error) {
        res.status(BAD_REQUEST).json(
            statusMessage(
                BAD_REQUEST,
                "An error during the object modification ocurred",
                err.message
            )
        );
    });
});


/**
 * Deletes a Task
 * @deprecated
 */
controller.delete("/:_id", function(req: Request, res: Response) {
    taskRepository.findByIdAndRemove(req.params["_id"]).exec().then(function (deletedTask: Task) {
        if(!deletedTask) 
            res.status(NOT_FOUND).json(
                statusMessage(NOT_FOUND, "The Task you were trying to delete was not found")
            );
        else 
            res.json(statusMessage(OK, "The requested Task was successfully deleted", deletedTask));
    }).catch(function(err: Error) {
        res.status(INTERNAL_SERVER_ERROR).json(internalServerErrorMessage(err));
    });
});


/**
 * Deletes all tasks
 * that have an after-running state
 */
controller.delete("/", function(_req: Request, res: Response) {
    taskRepository.deleteMany({
        status: { $gt: TaskStatus.RUNNING }
    }).then(function() {
        res.json(statusMessage(
            OK, 
            `All termintated tasks objects were removed from the database.`,
            `All termintated tasks objects were removed from the database.`
        ));
    });
});


/**
 * Gets all Tasks
 */
controller.get("/", function(req: Request, res: Response) {
    taskRepository.find().lean().then(function(tasks: any[]) {
        return tasks.map(function(task) {
            task.statusString = statusString(task.status);
            return task;
        });
    }).then(function(tasks) {
        res.json(tasks);
    });
});

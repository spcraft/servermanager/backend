import { getRequestUser, User } from "../middlewares/authentication";
import { Request, Response, Router } from "express";
import templateRepository, { Template } from "../models/Template";
import { validateHost as validateHost } from "../models/Host";
import { NOT_FOUND, BAD_REQUEST, INTERNAL_SERVER_ERROR, OK } from "http-status-codes";
import { statusMessage, internalServerErrorMessage } from "../util/messages";


const controller: Router = Router();
export default controller;


/**
 * Creates a new Template
 */
controller.post("/", function(req: Request, res: Response) {
    req.body["author"] = getRequestUser(req).uid; // Setting the templates's author as this user

    validateHost(req.body["host"]).then(function() {
        return templateRepository.create(req.body)
    }).then(function(newTemplate: Template) {
        res.json(newTemplate);
    }).catch(function(err: Error) {
        res.status(BAD_REQUEST).json(
            statusMessage(BAD_REQUEST, "There was an error in processing your request", err)
        );
    });
});


/**
 * Gets an information about the Template
 */
controller.get("/:_id", function(req: Request, res: Response) {
    templateRepository.findById(req.params["_id"]).exec().then(function(template: Template) {
        if(!template) 
            res.status(NOT_FOUND).json(
                statusMessage(NOT_FOUND, "The Template you were trying to get was not found")
            );
        else res.json(template);
    }).catch(function(err: Error) {
        res.status(INTERNAL_SERVER_ERROR).json(
            internalServerErrorMessage(err)
        );
    });
});


/**
 * Modifies a Template
 */
controller.put("/:_id", function(req: Request, res: Response) {
    templateRepository.findById(req.params["_id"]).exec().then(function(found) {
        if(!found) {
            res.status(NOT_FOUND).json(
                statusMessage(NOT_FOUND, "The Template you were trying to update was not found")
            );
            return;
        }
        Object.assign(found, req.body);
        return found.save();
    }).then(function(updated: Template) {
        res.json(
            statusMessage(
                OK, 
                "The requested Template was successfully updated, and the new version was sent back", 
                updated
            )
        )
    }).catch(function(err: Error) {
        res.status(BAD_REQUEST).json(
            statusMessage(
                BAD_REQUEST,
                "An error during the object modification ocurred",
                err.message
            )
        );
    });
});


/**
 * Deletes a Template
 */
controller.delete("/:_id", function(req: Request, res: Response) {
    templateRepository.findByIdAndRemove(req.params["_id"]).exec().then(function (deletedTemplate: Template) {
        if(!deletedTemplate) 
            res.status(NOT_FOUND).json(
                statusMessage(NOT_FOUND, "The Template you were trying to delete was not found")
            );
        else 
            res.json(statusMessage(OK, "The requested Template was successfully deleted", deletedTemplate));
    }).catch(function(err: Error) {
        res.status(INTERNAL_SERVER_ERROR).json(internalServerErrorMessage(err));
    });
});


/**
 * Gets all Templates
 */
controller.get("/", function(req: Request, res: Response) {
    templateRepository.find().then(function(templates) {
        res.json(templates);
    });
});
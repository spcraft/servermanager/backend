import { getRequestUser, User } from "../middlewares/authentication";
import { Request, Response, Router } from "express";
import hostRepository from "../models/Host";
import { internalServerErrorMessage } from "../util/messages";


const controller: Router = Router();
export default controller;



/** Gets the data about the current user */
controller.get("/", function(req: Request, res: Response) {
    const user: User = getRequestUser(req);
    res.status(200).send(user);
});


/** Gets the list of host ids that belong to this user */
controller.get("/hosts", function(req: Request, res: Response) {
    const user: User = getRequestUser(req);
    hostRepository.find({ owner: user.uid }).distinct("_id").exec().then(function(ids: string[]) {
        res.json(ids);
    }).catch(function(err: Error) {
        res.json(internalServerErrorMessage(err));
    });
});





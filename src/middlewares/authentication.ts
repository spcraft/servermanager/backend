import { verify, VerifyErrors } from "jsonwebtoken";
import { Request, Response } from "express";
import { UNAUTHORIZED, FORBIDDEN, TEMPORARY_REDIRECT } from "http-status-codes";
import config from "config";
import { statusMessage as statusMessage } from "../util/messages";


/**
 * User data
 */
export interface User {
    readonly cn            : string;
    readonly uid           : string;
    readonly uidNumber     : string;
    readonly homeDirectory : string;
    readonly loginShell    : string;
    readonly gidNumber     : string;
    readonly mail          : string;
}


/**
 * Fails the authorization with the given message
 * @param res response
 * @param message message
 */
function failAuthorization(req: Request, res: Response, message: string) {
    if(req.accepts("html"))
        res.redirect(config.get("app.authServerAddress"));
    else
        res.status(UNAUTHORIZED).send(statusMessage(UNAUTHORIZED, message));
}


/**
 * Fails the action with FORBIDDEN status
 * @param res response
 * @param message message
 */
export function denyPermission(res: Response, message?: string) {
    if(!message)
        message = "You do not have enough permissions to perform this action.";
    res.status(FORBIDDEN).send({
        status: FORBIDDEN,
        message: message
    });
}

/**
 * Gets the user, which performs this request.
 * This function has to be called only within the routers, 
 *   which are used with the authentication middleware
 * @param req the authenticated request, which contains the user token
 * @returns the user object, extracted from this token
 */
export function getRequestUser(req: Request): User {
    return req["request-user"] as User;
}


/**
 * The authentication middleware
 */
export default function authentication(): (Request, Response, Function) => void {
    
    /**
     * Given a request, a response, and the next function.
     * If the given request contains token inside the request Authorization header,
     *   or in the cookie with the name "token", and this token is valid for this 
     *   application -- call next.
     * Else do not call next, but return an unauthorized code.
     */
    return function(req: Request, res: Response, next: () => void) {
        let authorizationToken: string = null;
        if(authorizationToken = req.headers["authorization"] || req.cookies["token"])
            verify(
                authorizationToken, 
                config.get("app.secret"), 
                function(err: VerifyErrors, decoded: object) {
                    if(err)
                        failAuthorization(req, res, err.message);
                    else {
                        req["request-user"] = decoded;
                        next();
                    }
                }
            );
        else 
            failAuthorization(req, res, "You are not authorized to perform this action.");
    };
}

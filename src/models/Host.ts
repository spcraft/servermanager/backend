import { prop, Typegoose, pre, post } from "typegoose-es5";
import { validateModelEntity } from "../util/model";
import * as ping from "ping";
import NodeSSH from "node-ssh";
import templateRepository, { USER_QUEUE_TEMPLATE_TITLE } from "./Template";


/** A path to the private key file */
const PRIVATE_KEY_PATH: string = "keys/id_rsa";


/**
 * Checks if the Host is avaliable at a given address
 * @param address the Host address to be checked
 */
export function testServerConnection(address: string): Promise<void> {
    return new Promise<void>(function(resolve: Function, reject: Function) {
        ping.sys.probe(address, function(isAlive: boolean, err: Error) {
            if(err) {
                return reject(err);
            }
            if(isAlive) {
                return resolve();
            }
            reject(new Error(`The Host at ${ address } cannot be reached`));
        });
    });
}

export interface HostSearchQuery {
    responseType: "id" | "host";
    owner?      : string;
    nameLike?   : string;
}


/**
 * Finds hosts, which satisfy the conditions inside the query, 
 * and returns them in the format, specified by query
 * @param query the search query object
 */
export function findHosts(query: HostSearchQuery): Promise<Host[] | string[]> {
    /* ---- CONSTRUCTING SEARCH QUERY ---- */
    const searchQuery: any = new Object();
    if(query.owner)    searchQuery["owner"] = query.owner;
    if(query.nameLike) searchQuery["name"]  = new RegExp(query.nameLike, "i");

    /* ---- PERFORMING SEARCH QUERY ---- */
    let document = hostRepository.find(searchQuery);
    switch(query.responseType) {
    case "host":
        break;
    case "id":
        document = document.distinct("_id");
        break;
    default:
        return Promise.reject(
            new Error(
                `findHost: Invalid response type. 
                The value should be either 'id' or 'host'`
            )
        );
    }
    
    /* ---- RETURNING PROMISE TO RETURN THE HOSTS OR THEIR IDS ---- */
    return document.exec();
}


/**
 * Checks if the private key is valid for the root user of the Host
 * @param address the Host address to be checked
 * @param sshPort the ssh port (optional, 22 by default)
 */
function testPrivateKey(address: string, sshPort?: string | number): Promise<void> {
    return new Promise<void>(function(resolve: Function, reject: Function) {
        const connection = new NodeSSH();
        connection.connect({
            host: address,
            port: sshPort || 22,
            username: "root",
            privateKey: PRIVATE_KEY_PATH
        }).then(function() {
            connection.dispose();
        }).then(function() {
            resolve();
        }).catch(function(err: Error) {
            reject(err);
        });
    });
}

/** Describes the host's power control */
export enum PowerControlLevel {
    NONE           = 0, // THE HOST IS NOT CONTROLLED BY THE POWER SCHEDULER
    TASK_SCHEDULER = 1, // THE HOST IS CONTROLLED BY THE POWER SCHEDULER
    USER_QUEUE     = 2, // THE HOST IS CONTROLLED BY THE POWER SCHEDULER 
                        // AND THE MANAGER USERS
}


/**
 * A Machine (Server) model
 */
@pre<Host>("save", function(next: Function) {
    const promises: Promise<void>[] = [];
    const host = this;

    promises.push(
        // THE NEW HOST, AS WELL AS THE HOST WITH MODIFIED ADDRESS OR PORT,
        // HAS TO BE ONLINE AND ITS KEY HAS TO BE ACCEPTED BY THE SERVER
        new Promise<void>(function(resolve: Function, reject: Function) {
            if (!host.isNew && 
                !host.isModified("sshPort") &&
                !host.isModified("address")) return resolve();
            testServerConnection(host.address).then(function() {
                return testPrivateKey(host.address, host.sshPort);
            }).then(function() {
                resolve();
            }).catch(function(err: Error) {
                reject(err);
            });
        })
    );

    promises.push(
        // THE HOST HAS TO ALWAYS HAVE THE SAME OWNER
        new Promise<void>(function(resolve: Function, reject: Function) {
            if(!host.isNew && host.isModified("owner")) 
                return reject(new Error("The host owner cannot be changed."));
            resolve();
        })
    );

    Promise.all(promises).then(() => next()).catch((err: Error) => next(err));
})
@post<Host>("save", function(host) {
    // CREATING USER QUEUE TEMPLATE,
    // IF THE POWER CONTROL LEVEL IS MORE 
    // OR EQUAL TO USER_QUEUE
    if(host.powerControlLevel >= PowerControlLevel.USER_QUEUE)
        templateRepository.findOne({ 
            host : host._id,
            title: USER_QUEUE_TEMPLATE_TITLE,
        }).then((template) => {
            if(template) return;
            return templateRepository.create({
                author: host.owner,
                title : USER_QUEUE_TEMPLATE_TITLE,
                host  : host._id,
            });
        });
    else
        templateRepository.findOneAndRemove({
            host : host._id,
            title: USER_QUEUE_TEMPLATE_TITLE,
        }).catch(() => {});
})
@pre<Host>("remove", function(next: Function) {
    // CASCADING DELETE TO THE HOST'S TEMPLATES
    templateRepository.remove({ host: this._id }).then(() => {
        next();
    }).catch((err: Error) => {
        next(err);
    }); 
})
export class Host extends Typegoose {

    /* ---- BASIC INFORMATION ---- */
    /** Owner's username */
    @prop({ required: true })
    owner: string;

    /** The name of this machine */
    @prop({ required: true })
    name: string;

    /** The description of this machine */
    @prop({ required: false })
    description?: string;

    /** The server status (this is not managed by user) */
    @prop({ required: true, default: true })
    isOnline: boolean;


    /* ---- SSH CONNECTION INFORMATION ---- */
    /** The host address */
    @prop({ required: true })
    address: string;

    /** The ssh port of the host */
    @prop({ required: true, default: 22 })
    sshPort: number;


    /* ---- POWER CONTROL INFORMATION ---- */
    /** The host's power control level */
    @prop({ required: true, default: PowerControlLevel.NONE })
    powerControlLevel: PowerControlLevel;

    /** Poweroff command for shutting down the Host through ssh */
    @prop({ 
        required: function() { return this.powerControlLevel > 0 },
        validate: (value) => /^(?!\s*$).+/.test(value),
    })
    sshPoweroffCommand: string;

    /** Wake On Lan host's MAC-address */
    @prop({ 
        required: () => this.powerControlLevel > 0,
        validate: (value) => /^([0-9A-F]{2}[:-]){5}([0-9A-F]{2})$/.test(value),
    })
    wolMacAddress?: string;

    /** Wake On Lan host's IP-address */
    @prop({ required: false })
    wolIpAddress?: string;
}

/** Model Repository export */
const hostRepository = new Host().getModelForClass(Host);
export type HostRepository = typeof hostRepository;
export default hostRepository;


/**
 * Asynchronously checks if the given host is valid
 * @param _id the host id
 */
export function validateHost(_id: string): Promise<void> {
    return validateModelEntity(hostRepository, "Host", _id);
}
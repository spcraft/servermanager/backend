import { prop, Typegoose, Ref, pre, post } from "typegoose-es5";
import { Template } from "./Template";
import { validateModelEntity } from "../util/model";


/** 
 * A Status of the Task
 */
export enum TaskStatus {
    PENDING          = 0, // WHEN IS WAITING FOR ITS EXECUTION TIME
    READY            = 1, // WHEN IS PUT ONTO A READY QUEUE (SMALL TIME BEFORE EXEC)
    RUNNING          = 2, // EXECUTION STAGE
    COMPLETED        = 3, // WHEN THE EXECUTION WAS SUCCESSFUL
    CANCELLED        = 4, // WHEN THE EXECUTION WAS CANCELLED BEFORE THE RUNNING STAGE
    KILLED_BY_USER   = 5, // WHEN THE EXECUTION WAS INTERRUPTED BY THE USER 
    KILLED_BY_SYSTEM = 6  // WHEN THE EXECUTION WAS INTERRUPTED BY THE SYSTEM
}

/**
 * Gets the Task status in a string format
 */
export function statusString(status: TaskStatus): string {
    switch(status) {
        case TaskStatus.PENDING         : return "PENDING";
        case TaskStatus.READY           : return "READY";
        case TaskStatus.RUNNING         : return "RUNNING";
        case TaskStatus.COMPLETED       : return "COMPLETED";
        case TaskStatus.CANCELLED       : return "CANCELLED"; 
        case TaskStatus.KILLED_BY_USER  : return "KILLED_BY_USER";
        case TaskStatus.KILLED_BY_SYSTEM: return "KILLED_BY_SYSTEM";
        default                         : return "UNKNOWN";
    }
}


/**
 * A Task
 */
export class Task extends Typegoose {

    /* ---- FIELDS ---- */
    /** The user identifier or a username of the task requester */
    @prop({ required: true })
    public readonly user: string;
    
    /** A template, which holds the command and privilege types of the task */
    @prop({ required: true, ref: Template })
    public readonly template: Ref<Template>;

    /** The date this task starts */
    @prop({ 
        required: true, 
        default: new Date(Date.now() + 10000),
        validate: function(date: Date) {
            if(this.isNew)
                return date.getTime() >= new Date().getTime()
            return true;
        }
    })
    public readonly startDate: Date;

    /** The date this task ends */
    @prop({
        required: false,
        validate: {
            validator: function(date: Date) { return date > this.startDate },
            message: `The endDate cannot be before than the startDate`
        }
    })
    public readonly endDate?: Date;

    /** Current status of this task */
    @prop({ 
        required: true, 
        default: TaskStatus.PENDING 
    })
    public readonly status: TaskStatus;
}

/** Model Repository export */
const taskRepository = new Task().getModelForClass(Task);
export type TaskRepository = typeof taskRepository;
export default taskRepository;


/**
 * Asynchronously checks if the given task is valid
 * @param _id the task id
 */
export function validateTask(_id: string): Promise<void> {
    return validateModelEntity(taskRepository, "Task", _id);
}

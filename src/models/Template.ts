import { prop, Typegoose, Ref, pre } from "typegoose-es5";
import { Host } from "./Host";
import { validateModelEntity } from "../util/model";


/** A Special kind of a template, which does not have any command */
export const USER_QUEUE_TEMPLATE_TITLE: string = "___user_queue___";

/**
 * A Task Template
 */
@pre<Template>("save", function(next) {
    if(!this.isNew && this.isModified("author"))
        next(new Error("The template author cannot be changed."));
    else next();
})
export class Template extends Typegoose {

    /* ---- SCHEMA PROPERTIES ---- */
    /** The Template's author */
    @prop({ required: true })
    public author: string;

    /** The Task's (and template's) title */
    @prop({
        required: true,
        validate: {
            validator: function() {
                const { host, title, _id } = this;
                return templateRepository.find({ host, title }).then(function(arr) {
                    for(const template of arr)
                        if(template._id !== _id)
                            return false;
                    return true;
                });
            },
            message: "The combination of Host and Template title should be unique."
        }
    })
    public title: string;
    
    /** The Task's command */
    @prop({ required: function() { return this.title !== USER_QUEUE_TEMPLATE_TITLE } })
    public command?: string;

    /** Determines if this is a root user command */
    @prop({ required: true, default: false })
    public isRoot: boolean;

    /** Determines the Host, on which the command is executed */
    @prop({ required: true, ref: Host })
    public host: Ref<Host>;
}

/** Model Repository export */
const templateRepository = new Template().getModelForClass(Template);
export type TemplateRepository = typeof templateRepository;
export default templateRepository;

/**
 * Asynchronously checks if the given template is valid
 * @param _id the template id
 */
export function validateTemplate(_id: string): Promise<void> {
    return validateModelEntity(templateRepository, "Template", _id);
}

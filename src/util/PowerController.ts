import hostRepository, { PowerControlLevel } from "../models/Host";
import populateTask, { PopulatedTask } from "./populateTask";
import logger from "./logger";
import taskRepository from "../models/Task";

/** A function, which is called to power on or off the server */
type PowerCallbackFunction  = (hostId: string) => void;

/**
 * A Host representation within
 * the PowerController.
 */
class HostPowerComponent {
    /* ---- FIELDS ---- */
    /** A Host id  */
    private readonly hostId      : string;
    /** A set of currently running Tasks */
    private readonly runningTasks: Set<string>;
    /** A poweron callback function */
    private readonly poweron     : () => void;
    /** A poweroff callback function */
    private readonly poweroff    : () => void;
    
    /* ---- METHODS ---- */
    /**
     * Adds the task to the component
     * @param taskId the _id of the task
     * @returns a function, which, if called, deletes the task from the runningTasks set
     */
    public addTask(taskId: string): () => void {
        logger.debug(`Added Task#${ taskId } to the PowerController.`)
        this.runningTasks.add(taskId);
        return () => {
            logger.debug(`Removed Task#${ taskId } from the PowerController.`);
            this.runningTasks.delete(taskId);
        };
    }

    public schedulePowerCheck(): Promise<void> {
        logger.debug(`Scheduling the power check for the Host#${ this.hostId }...`)
        return hostRepository.findById(this.hostId).then(host => {
            if(host.isOnline && this.runningTasks.size === 0)
                return this.poweroff();
            if(!host.isOnline && this.runningTasks.size > 0)
                return this.poweron();
        }).then(() => {
            logger.debug(`...Done power check for Host#${ this.hostId }`);
        });
    }

    /* ---- CONSTRUCTOR ---- */
    /**
     * Default constructor
     * @param hostId the id of the host to be controlled by this cmponent
     * @param poweronCallback the callback to power the host on
     * @param poweroffCallback the callback to power the host off
     */
    constructor(hostId: string, 
                poweronCallback: PowerCallbackFunction, 
                poweroffCallback: PowerCallbackFunction) {
        this.hostId       = hostId;
        this.runningTasks = new Set<string>();
        this.poweron      = (() => poweronCallback(this.hostId));
        this.poweroff     = (() => poweroffCallback(this.hostId));
    }
}


/** 
 * The power controller class.
 * It is responsible for keeping in mind, 
 * which Hosts are being used, and which are not.
 * 
 * It is also responsible for waking up, and powering
 * down the hosts, whose powerControlLevel is higher
 * than NONE, depending on their tasks.
 */
export class PowerController {
    /* ---- FIELDS ---- */
    /** A map of host _ids to HostPowerComponents */
    private readonly hostMap: { [_id: string]: HostPowerComponent };
    /** A poweron callback function */
    private readonly poweronCallback : PowerCallbackFunction;
    /** A poweroff callback function */
    private readonly poweroffCallback: PowerCallbackFunction;

    /* ---- METHODS ---- */
    /**
     * Starts tracking this task. If the server has a task, it should be powered on.
     * @param task the populated task, which should be tracked by the PowerController.
     * @returns the function, which, if called, stops tracking the task (it is considered to be finished).
     */
    public addTask(task: string | PopulatedTask): Promise<() => void> {
        return (typeof task === "string" ? 
        taskRepository.findById(task).then(populateTask) : 
        Promise.resolve(task)).then(populatedTask => {
            if(!this.hostMap[populatedTask.template.host])
                return this.scheduleHostMapUpdate().then(() => 
                    this.addTask(populatedTask)
                );
            else {
                return this.hostMap[
                    populatedTask.template.host
                ].addTask(populatedTask._id);
            }
        });
    }

    /**
     * Schedules the check of the poweron state of every
     * Host, which is under the control of this PowerController.
     * If the Host is online, but contains no job -- it is powered off.
     * If the Host is offline, but contains at least a job -- it is powered on.
     * @returns A Promise that the action above will be completed.
     */
    public schedulePowerCheck(): Promise<void> {
        logger.debug("Scheduling the power check...");
        return Promise.all(
            Object.entries(this.hostMap).map(([, component]) => 
                component.schedulePowerCheck()
            )
        ).then(() => {
            logger.debug("...Done the power check");
        });
    }

    /**
     * Schedules the HostMap update. This adds the new hosts.
     * @returns A Promise that the action above will be completed.
     */
    public scheduleHostMapUpdate(): Promise<void> {
        logger.debug("Scheduling the host map update...");
        return hostRepository.find({ 
            powerControlLevel: { $gt: PowerControlLevel.NONE } 
        }).distinct("_id").then(
            (hostIds: string[]) => hostIds.forEach((hostId) => {
                if(this.hostMap[hostId]) return;
                this.hostMap[hostId] = new HostPowerComponent(
                    hostId,
                    this.poweronCallback,
                    this.poweroffCallback
                );
            })
        ).then(() => { 
            logger.debug(`...Done updating the host map`)
        });
    }

    /* ---- CONSTRUCTOR ---- */
    constructor(poweronCallback : PowerCallbackFunction, 
                poweroffCallback: PowerCallbackFunction) {
        logger.info(`Initialized the host PowerController.`);
        /* ---- INITIALIZING FIELDS ---- */
        this.poweronCallback  = poweronCallback;
        this.poweroffCallback = poweroffCallback;
        this.hostMap = {};
        /* ---- FILLING THE HOST MAP ---- */
        this.scheduleHostMapUpdate();
    }
}
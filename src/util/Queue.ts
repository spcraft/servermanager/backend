
/**
 * A Queue node
 */
class QueueNode<T> {
    /* ---- FIELDS ---- */
    /** The value of the node */
    public readonly value: T;
    /** The next element in the queue */
    public next: QueueNode<T> | undefined;

    /* ---- CONSTRUCTOR ---- */
    /**
     * Default constructor
     * @param value a value of the node
     * @param next the next node
     */
    constructor(value: T, next?: QueueNode<T>) {
        this.value = value;
        this.next = next;
    }
}


/**
 * A Queue implementation
 */
export default class Queue<T> {
    /* ---- FIELDS ---- */
    /** The first node in a queue */
    private first: QueueNode<T> | undefined;
    /** The last node in a queue */
    private last : QueueNode<T> | undefined;
    /** The queue length */
    private queueLength: number;

    /* ---- PSEUDO FIELDS ---- */
    /** Gets the length of the queue */
    public get length(): number { return this.queueLength; }
    /** Gets the empty status of the queue */
    public get isEmpty(): boolean { return this.length == 0; }

    /* ---- METHODS ---- */
    
    /**
     * Pops the first element in the queue.
     * @returns the first element in the queue, or undefined, if queue is empty.
     */
    public pop(): T | undefined {
        if(this.isEmpty)
            return undefined;
        const returnedElement: T = this.first.value;
        if(this.length == 1)
            this.last = undefined;
        this.first = this.first.next;
        --this.queueLength;
        return returnedElement;
    }

    /**
     * Pushes the new element to the end of a queue.
     * @param element the new last element of a queue.
     */
    public push(element: T): void {
        if(this.isEmpty)
            this.first = this.last = new QueueNode(element);
        else {
            this.last.next = new QueueNode(element);
            this.last = this.last.next;
        }
        ++this.queueLength;
    }

    /**
     * Inserts the element in a sorted way, according to the compareFunction.
     * If there is a subqueue, whose elements are equal to the argument, the
     * argument is inserted in the end of this subqueue.
     * @param element the element to be inserted.
     * @param compareFunction The function, which is used to compare elements.
     *                        The function returns zero, if elements *A* and *B* are equal.
     *                        The function returns negative number, if *A* comes earlier than *B*.
     *                        The function returns positive number, if *B* comes earlier than *A*.
     * @returns the saved element
     */
    public insertSorted(element: T, compareFunction?: (a: T, b: T) => number): T {
        if(!compareFunction) 
            compareFunction = (a, b) => a < b ? -1 : 1;

        if(this.isEmpty || compareFunction(this.last.value, element) <= 0)
            // IF THERE IS AN EMPTY QUEUE, OR THE ELEMENT COMES LATER THAN LAST -> PUSH
            this.push(element);
        else if(compareFunction(this.first.value, element) > 0)
            // IF THE ELEMENT COMES EARLIER THAN FIRST -> PUSH FRONT
            this.first = new QueueNode(element, this.first);
        else {
            let nodePointer: QueueNode<T> = this.first;
            while(compareFunction(nodePointer.next.value, element) <= 0)
                nodePointer = nodePointer.next;
            nodePointer.next = new QueueNode(element, nodePointer.next);
            ++this.queueLength;
        }
        return element;
    }

    /**
     * Applies the callback on every of the remaining element in the queue, while popping them.
     * @param callbackfn the callback, which is applied on every of the remaining element.
     *                   Contains 3 parameters: element, index, and a function, which,
     *                   if called, stops the iteration without popping the current element
     *                   from the queue.
     */
    public forEachRemaining(callbackfn: (element: T, index: number, endIteration: Function) => void): void {
        let iterationNumber: number = 0;
        let endIteration: boolean = false;
        while(this.first) {
            callbackfn(
                this.first.value,
                iterationNumber++,
                () => { endIteration = true; }
            );
            if(endIteration) break;
            this.pop();
        }
    }


    /* ---- CONSTRUCTOR ---- */
    /**
     * Default constructor
     */
    constructor() {
        this.queueLength = 0;
        this.first = undefined;
        this.last = undefined;
    }
}
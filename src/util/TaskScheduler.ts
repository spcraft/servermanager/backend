import taskRepository, { TaskStatus } from "../models/Task";
import Queue from "./Queue"; 
import logger from "./logger";


/** The element of a readyQueue in the TaskScheduler */
declare type ReadyQueueElement = { 
    _id              : string, 
    startDate        : Date, 
    isCancelled      : boolean, 
    isPoppedFromQueue: boolean 
};

/** The task load handler */
declare type TaskLoadHandler    = (taskId: string, cancelTask: () => Promise<void>) => Promise<void>;

/** The task execute handler */
declare type TaskExecuteHandler = (taskId: string) => Promise<void>;



/** The task executor class */
export class TaskScheduler {
    /* ---- TASK QUEUES AND SETS ---- */
    /** A ready queue of tasks */
    private readonly readyQueue    : Queue<ReadyQueueElement>;
    /** A set of already fetched task ids */
    private readonly taskHistorySet: Set<string>;


    /* ---- METHODS AND PSEUDO FIELDS ---- */
    /** A task handler, assigned externally. Is called on task execution */
    private readonly executeTask   : TaskExecuteHandler;
    /** A task handler, assigned externally. Is called on task load */
    private readonly loadTask      : TaskLoadHandler;
    /** Cancels the loaded task. The task has to be in READY state */
    private cancelTask(queueElement: ReadyQueueElement): Promise<void> {
        logger.debug(`Cancelling Task${ queueElement._id }...`);
        return taskRepository.findById(queueElement._id).then(task => {
            if(task.status >= TaskStatus.RUNNING || queueElement.isPoppedFromQueue)
                return Promise.reject(new Error(
                    `...The Task#${ task._id } cannot be cancelled ` +
                    `because it is already running or was terminated`
                ));
            queueElement.isCancelled = true;
            return Object.assign(task, { status: TaskStatus.CANCELLED }).save();
        }).then(() => {
            logger.debug(`...Cancelled Task${ queueElement._id }`);
        }).catch((err: Error) => {
            logger.error("Unexpected bahavior in TaskScheduler::cancelTask");
            logger.error(err.message);
            logger.error(err.stack);
            process.exit(-1);
        });
    }
    


    /**
     * Asynchronously loads the tasks that have 
     * NON-TERMINAL status (i.e. PENDING, READY, RUNNING),
     * and that are going to be executed within the
     * next prefetchDeltaTime milliseconds.
     * @param prefetchDeltaTime the time interval between Date.now
     *                          and the maximum possible startTime
     *                          of the prefetched task (in milliseconds).
     * @returns A Promise that the tasks will be fetched. 
     *          This promise is mainly used for catching errors.
     */
    public scheduleTaskFetch(prefetchDeltaTime: number): Promise<void> {
        logger.debug(`Fetching the tasks from the database to the TaskScheduler...`);
        return taskRepository.find({
            /* -- STAGE 0: TASK FETCHING -- */
            status:    { $in: [ TaskStatus.PENDING, TaskStatus.READY, TaskStatus.RUNNING ] },
            startDate: { $lte: new Date(Date.now() + prefetchDeltaTime) }
        }).then(tasks => tasks.filter(task => {
            /* -- STAGE 1: FILTERING REPEATING TASKS -- */
            return !this.taskHistorySet.has(task._id.toString());
        })).then(tasks => tasks.map(task => {
            /* -- STAGE 2: ADDING TASKS TO HISTORY -- */
            this.taskHistorySet.add(task._id.toString());
            return task;
        })).then(tasks => tasks.map(task => 
            /* -- STAGE 3: ADDING isCancelled AND isPoppedFromQueue PROPERTIES -- */
            Object.assign(task, { isCancelled: false, isPoppedFromQueue: false }
        ))).then(tasks => Promise.all(tasks.map(task =>
            /* -- STAGE 4: LOADING ALL TASKS, PROVIDING CANCELL CALLBACKS -- */
            this.loadTask(task._id, () => this.cancelTask(task))
        )).then(() => tasks)).then(tasks => Promise.all(tasks.map(task =>
            /* -- STAGE 5: SETTING EVERY PENDING TASK TO READY STATE -- */
            task.status === TaskStatus.PENDING ?
            Object.assign(task, { status: TaskStatus.READY }).save() :
            task
        ))).then(tasks => tasks.forEach(task => {
            /* -- STAGE 6: ADDING ALL THE TASKS TO THE QUEUE FOR PROCESSING -- */
            this.readyQueue.insertSorted(
                task,
                (a, b) => a.startDate < b.startDate ? -1 : 1
            )
        })).then(() => {
            logger.debug(`...Done fetching the tasks from the database to the TaskScheduler`);
        });
    }

    /**
     * Asynchronously traverses the queue, setting all of the READY tasks,
     * whose startTime is less or equal to current datetime,
     * to RUNNING state, launching their execution.
     * @returns A Promise that the required tasks will be launched.
     *          This promise is mainly used for catching errors.
     */
    public scheduleTaskExecution(): Promise<void> {
        logger.debug(`Fetching the tasks from the scheduler's ready queue to running queue...`);
        const promises: Promise<any>[] = [];
        const currentDate: Date = new Date();
        this.readyQueue.forEachRemaining((element, _index, stopIteration) => {
            if(element.isCancelled) return;
            if(element.isPoppedFromQueue) return;
            if(currentDate.getTime() < element.startDate.getTime())
                return stopIteration();
            promises.push(taskRepository.findById(element._id).then(task =>
                Object.assign(task, { status: TaskStatus.RUNNING }).save()
            ).then(task => this.executeTask(task._id)));
            element.isPoppedFromQueue = true;
        });
        return Promise.all(promises).then(() => {
            logger.debug(`...Done fetching the tasks from the scheduler's ready queue to running queue.`);
        });
    }


    /* ---- CONSTRUCTOR ---- */
    constructor(onTaskLoad: TaskLoadHandler, onTaskExecute: TaskExecuteHandler) {
        logger.info(`Initialized task scheduler.`);

        /* ---- EXECUTORS ---- */
        this.executeTask = onTaskExecute;
        this.loadTask    = onTaskLoad;
        /* ---- TASK QUEUES ---- */
        this.readyQueue     = new Queue();
        this.taskHistorySet = new Set<string>();
    }
}
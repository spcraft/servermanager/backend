import hostRepository, { Host, testServerConnection } from "../models/Host";
import { promise } from "ping";
import logger from "./logger";
import config from "config";


/**
 * 
 * @param hostId The id of a host
 */
export function updateHostOnlineStatus(hostId: string): Promise<Host> {
    return new Promise<Host>(function(resolve: Function, reject: Function) {
        function saveHost(host): void {
            host.save().then(function() {
                resolve();
            }).catch(function(err: Error) {
                reject(err);
            });
        }

        hostRepository.findById(hostId).exec().then(function(host) {
            testServerConnection(host.address).then(function() {
                if(host.isOnline) return resolve();
                host.isOnline = true;
                saveHost(host);
            }).catch(function() {
                if(!host.isOnline) return resolve();
                host.isOnline = false;
                saveHost(host);
            });
        })
    });
}



/**
 * Updates the online status of every Host
 */
function updateHostsOnlineStatuses(): Promise<void[]> {
    logger.debug("Updating online status of every host...");
    const promises: Promise<void>[] = [];
    hostRepository.find().exec().then(function(hosts) {
        hosts.forEach(function(host) {
            promises.push(
                new Promise<void>(function(resolve: Function, reject: Function) {
                    function saveHost(host): void {
                        host.save().then(function() {
                            resolve();
                        }).catch(function(err: Error) {
                            reject(err);
                        });
                    }

                    testServerConnection(host.address).then(function() {
                        if(host.isOnline) return resolve();
                        host.isOnline = true;
                        saveHost(host);
                    }).catch(function() {
                        if(!host.isOnline) return resolve();
                        host.isOnline = false;
                        saveHost(host);
                    });
                })
            );
        });
    });
    return Promise.all(promises).then(function(promises) {
        logger.debug("...Hosts' online statuses were updated.");
        return promises;
    });
}



/**
 * A function, which updates the statuses every N millis
 */
export default function hostOnlineStatusUpdater(): () => void {
    logger.info("Initializing Host online status updater...");
    logger.info(
        `Set host status update period to ${ config.get("scheduler.hostOnlineCheckPeriod") } milliseconds.`
    );
    const interval: NodeJS.Timeout = setInterval(
        updateHostsOnlineStatuses, 
        config.get("scheduler.hostOnlineCheckPeriod")
    );
    logger.info("...Done initializing Host online status updater.");
    return function(): void {
        logger.info("Terminated Host online status updater.");
        clearInterval(interval);
    }
}


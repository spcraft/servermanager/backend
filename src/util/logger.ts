import winston from "winston";
import { format } from "logform";
import config from "config";
import * as fs from "fs";

if(!fs.existsSync(config.get("logger.logFolder")))
    fs.mkdirSync(config.get("logger.logFolder"));


const logger = winston.createLogger({
    format: format.combine(
        format.colorize(),
        format.timestamp(),
        format.align(),
        format.printf(info => `${info.timestamp} [${info.level}]: ${info.message}`)
    ),
    transports: [
        new winston.transports.Console({ 
            level: config.get("logger.stdoutLogLevel") 
        }),
        new winston.transports.File({ 
            dirname: config.get("logger.logFolder"),
            filename: `log_${ new Date().toISOString() }.txt`, 
            level: config.get("logger.fileLogLevel") 
        })
    ]
});
export default logger;

import { INTERNAL_SERVER_ERROR } from "http-status-codes";


/**
 * Creates a standartized message with a status. 
 * Should be used for simple response templates, when answering with in json format.
 * @param status the status code of the response
 * @param message the message of the response
 * @param content (optional) the object content of the response
 */
export function statusMessage(status: number, message: string, content?: any): object {
    return { status: status, message: message, content };
}

/**
 * Creates a standartized message with 500 status code
 * @param err an (optional) error object
 */
export function internalServerErrorMessage(err?: any): object {
    if(err instanceof Error) err = err.message;
    return statusMessage(INTERNAL_SERVER_ERROR, "An internal server error occured", err);
}
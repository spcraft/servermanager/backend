import { Model } from "mongoose";

/**
 * Asynchronously checks the model repository on the existence of the entity with the given id
 * @param model the model object
 * @param modelName the name of the model, which will be used in the rejection response
 * @param entityId the entity id to be checked in the repository
 */
export function validateModelEntity(model: Model<any>, modelName: string, entityId: string): Promise<void> {
    const REJECTION_MESSAGE: string = `The given ${ modelName } id is not valid`;
    return new Promise<void>(function(resolve: Function, reject: Function) {
        model.findById(entityId).lean().exec().then(function(value: any) {
            if(value) resolve();
            else reject(new Error(REJECTION_MESSAGE));
        }).catch(function() {
            reject(new Error(REJECTION_MESSAGE));
        });
    });
}



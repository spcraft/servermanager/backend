import { Task } from "../models/Task";
import { Template } from "../models/Template"
import hostRepository, { Host } from "../models/Host";
import { InstanceType } from "typegoose-es5";

/** A Task with a Template object inside */
export type PopulatedTask = 
    & Task 
    & { template: & Template & { host: string } }
    & { _id: string };

/**
 * Populates the given task with the template and host.
 * @param task the task to be populated.
 */
export default function populateTask(task  : InstanceType<Task>): Promise<PopulatedTask> {
    return task.populate({
            path: "template",
            // populate: { 
            //     path: "host",
            //     model: hostRepository
            // }
    }).execPopulate() as unknown as Promise<PopulatedTask>;
}

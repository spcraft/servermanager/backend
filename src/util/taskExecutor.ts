import hostRepository, { PowerControlLevel } from "../models/Host";
import templateRepository, { USER_QUEUE_TEMPLATE_TITLE } from "../models/Template";
import taskRepository, { TaskStatus, Task } from "../models/Task";
import { TaskScheduler } from "./TaskScheduler";
import populateTask, { PopulatedTask } from "./populateTask";
import { PowerController } from "./PowerController";
import { wake as wakeonlan } from "node-wol";
import { InstanceType } from "typegoose-es5";
import nodeSSH from "node-ssh";
import logger from "./logger";
import config from "config";

/* ---- TASK EXECUTOR ---- */
/** Starts the Task Scheduler and Executor subroutine */
export default function taskExecutor(): () => void {
    logger.info("Initializaing task executor...");
    logger.info(`Set task loading period to ${ config.get("scheduler.taskLoadPeriod") } milliseconds.`);
    logger.info(`Set task execution period to ${ config.get("scheduler.taskExecutePeriod") } milliseconds.`);
    logger.info(`Set task preloading delta to ${ config.get("scheduler.taskPreloadDelta") } milliseconds.`);
    logger.info(`Set power check period to ${ config.get("scheduler.powerCheckPeriod") } milliseconds.`);
    logger.info(`These parameters can me modified in the scheduler part of the app configuration.`);
    

    /* -- FIRST SYSTEM STATE -- */
    setImmediate(function() {
        taskScheduler.scheduleTaskFetch(
            config.get("scheduler.taskPreloadDelta")
        ).then(
            () => taskScheduler.scheduleTaskExecution()
        ).then(
            () => powerController.schedulePowerCheck()
        ).catch((err: Error) => logger.error(err.message));
    });

    /* -- SETTING THE RULES FOR SYSTEM UPDATE -- */
    const taskFetchInterval: NodeJS.Timeout = setInterval(
        () => taskScheduler.scheduleTaskFetch(
            config.get("scheduler.taskPreloadDelta")
        ), config.get("scheduler.taskLoadPeriod")
    );

    const taskExecuteInterval: NodeJS.Timeout = setInterval(
        () => taskScheduler.scheduleTaskExecution(),
        config.get("scheduler.taskExecutePeriod")
    );

    const powerControlInterval: NodeJS.Timeout = setInterval(
        () => powerController.schedulePowerCheck(),
        config.get("scheduler.powerCheckPeriod")
    );


    /* -- RETURNING THE FUNCTION WITH  -- */
    logger.info("...Done initializaing task executor.");
    return function() {
        clearInterval(taskFetchInterval);
        clearInterval(taskExecuteInterval);
        clearInterval(powerControlInterval);
        logger.info(`Terminated the task executor.`);
    }
}

/* ---- TASK COMPONENT ---- */
/** 
 * A task component, 
 * which encapsulates the actions that
 * can be done to the running task
 * 
 * Contains 3 functions: complete, kill, and cancel,
 * which are being set upon the construction.
 * 
 * It is guaranteed by this object, that any function
 * called on this object, will be called once.
 * 
 * If the second call occures the function is returned
 */
export class TaskComponent {
    public static readonly componentMap: { 
        [taskId: string]: TaskComponent 
    } = {};

    private isFinished   : boolean;
    private readonly onComplete : () => Promise<void>;
    private readonly onKill     : () => Promise<void>;
    private readonly onCancel   : () => Promise<void>;

    public complete(): Promise<void> {
        if(this.isFinished) return;
        this.isFinished = true;
        this.onComplete();
    }

    public kill(): Promise<void> {
        if(this.isFinished) return;
        this.isFinished = true;
        this.onKill();
    }

    public cancel(): Promise<void> {
        if(this.isFinished) return;
        this.isFinished = true;
        this.onCancel();
    }

    public readonly isTaskFinished = () => 
        this.isFinished;    
    
    /**
     * Default constructor
     * If an argument is 
     * @param onComplete A COMPLETE callback
     * @param onKill A KILLED_BY_SYSTEM callback
     * @param onCancel A KILLED_BY_USER or CANCELLED callback
     * @param isFinished An (optional) state, which is false by default 
     */
    constructor(onComplete : TaskComponent | (() => Promise<void>),
                onKill     : TaskComponent | (() => Promise<void>), 
                onCancel   : TaskComponent | (() => Promise<void>),
                isFinished?: TaskComponent | boolean) {
        this.isFinished = 
            isFinished ? 
            (isFinished instanceof TaskComponent ? 
                isFinished.isFinished : isFinished) : 
            false;
        this.onComplete = 
            onComplete instanceof TaskComponent ? 
            onComplete.onComplete : onComplete;
        this.onKill = 
            onKill instanceof TaskComponent ? 
            onKill.onKill : onKill; 
        this.onCancel = 
            onCancel instanceof TaskComponent ? 
            onCancel.onCancel : onCancel;
    }
}


/* ---- SUBROUTINE OBJECTS ---- */
/** An object, which is in charge of turning on and off the hosts */
const powerController: PowerController = new PowerController(
    onHostPoweron,
    onHostPoweroff
);

/** An object, which is in charge of fetching the tasks, and starting their execution */
const taskScheduler: TaskScheduler = new TaskScheduler(
    onTaskLoad,
    onTaskExecute
);



/* ---- CALLBACKS ---- */
/** A callback, which is called, when the host should be powered on */
function onHostPoweron(hostId: string): Promise<void> {
    logger.debug(`Attempting to power the Host#${ hostId } ON...`);
    return hostRepository.findById(hostId).then(host => new Promise((resolve, reject) =>
        host.powerControlLevel > PowerControlLevel.NONE ?
        wakeonlan(host.wolMacAddress, {
            address: host.wolIpAddress,
            port   : 9
        }, err => err? reject(err) : resolve(true)) :
        resolve(false)
    )).then(isControlledByPowerController => {
        if(isControlledByPowerController) 
            logger.debug(`...The Host#${ hostId } is now ON`);
        else 
            logger.debug(
                `...The Host#${ hostId } does not` +
                ` require poweron, ` +
                `because it is ALWAYS ON`
            );
    });
}

/** A callback, which is called, when the host should be powerer off */
function onHostPoweroff(hostId: string): Promise<void> {
    logger.debug(`Attempting to power the Host#${ hostId } OFF...`);
    const ssh = new nodeSSH();
    return hostRepository.findById(hostId).then(host => 
        new Promise<boolean>((resolve) =>
            host.powerControlLevel > PowerControlLevel.NONE ?
            ssh.connect({
                host      : host.address,
                port      : host.sshPort,
                username  : "root",
                privateKey: "keys/id_rsa",
            }).then(() => 
                ssh.execCommand(host.sshPoweroffCommand)
            ).then(() => resolve(true)) : resolve(false)
        )
    ).then((isControlledByPowerController) => {
        if(isControlledByPowerController)
            logger.debug(`...The Host#${ hostId } is now OFF.`);
        else
            logger.debug(
                `...The Host#${ hostId } does not` +
                ` require poweroff, ` +
                `because it is ALWAYS ON.`
            );
    });
}

/**
 * A callback, which is called, when the task enters READY stage
 * @param taskId The id of a task
 * @param cancelCallback A function, which, if called, cancells the task, 
 *                       if it is not already running or terminated
 */
function onTaskLoad(taskId: string, cancelCallback: () => Promise<void>): Promise<void> {
    logger.debug(`Loading the Task#${ taskId }...`);
    return taskRepository.findById(taskId).then(
        /* -- STAGE 1: POPULATING TASK -- */
        populateTask
    ).then(populatedTask => Promise.all([
        /* -- STAGE 2: FINDING HOST -- */
        populatedTask, hostRepository.findById(
            populatedTask.template.host
        )
    ])).then(([task, host]) =>
        /* -- STAGE 3: ADDING TO POWER COMPONENT  -- */
        host.powerControlLevel > PowerControlLevel.NONE ?
        powerController.addTask(task) :
        (() => {})
    ).then((removeTaskFromPowerComponent) => {
        /* -- STAGE 4: GENERATING TASK COMPONENT -- */
        const terminateTask = (status: TaskStatus) => () =>
            // THIS IS A HELPER FUNCTION
            taskRepository.findById(taskId).then(task =>
                Object.assign(task, { status: status }).save()
            ).then(removeTaskFromPowerComponent);
        TaskComponent.componentMap[taskId] = new TaskComponent(
            terminateTask(TaskStatus.COMPLETED),
            terminateTask(TaskStatus.KILLED_BY_SYSTEM),
            () => {
                removeTaskFromPowerComponent();
                return taskRepository.findById(taskId).then(task =>
                    task && task.status < TaskStatus.RUNNING ? 
                    cancelCallback() : Promise.resolve()
                );
            }
        );
    }).then(() => {
        logger.debug(`...Done loading the Task#${ taskId }`);
    });
}

/** A callback, which is called, when the task enters RUNNING stage */
function onTaskExecute(taskId: string): Promise<void> {
    /** A helper function, which sets the task's status as KILLED_BY_USER */
    const setTaskAsInterrupted = (taskId: string) => () => taskRepository.findById(taskId).then(task => 
        task.status !== TaskStatus.RUNNING ?
        Promise.reject(new Error("Trying to interrupt a task with a non-running state")) :
        Object.assign(task, { status: TaskStatus.KILLED_BY_USER }).save()
    );


    /** TaskAction performs a TaskComponent update, and a Task execution launch */
    type TaskAction = 
        (pTask: PopulatedTask) 
        => Promise<void>;
    const onTimerTask: TaskAction = task => new Promise(resolve => {
        if(TaskComponent.componentMap[task._id].isTaskFinished())
            return resolve();
        const completionTimeout: NodeJS.Timeout = setTimeout(
            resolve,
            task.endDate.getTime() - Date.now()
        );
        const prevComponent = TaskComponent.componentMap[task._id]
        TaskComponent.componentMap[task._id] = new TaskComponent(
            // THIS COMPONENT IS BASED ON THE ONE, 
            // WHICH WAS CREATED ON LOADING STAGE
            prevComponent,  // SAME COMPLETE
            () => {
                // KILL NOW ALSO REMOVES THE TIMEOUT
                clearTimeout(completionTimeout);
                return prevComponent.kill.bind(prevComponent)().then(resolve);
            },
            () => {
                // CANCEL IS NOW KILLING, AND REMOVING THE TIMEOUT
                clearTimeout(completionTimeout);
                // return prevComponent.cancel.bind(prevComponent)().then(
                //     setTaskAsInterrupted(taskId)
                // ).then(resolve);
                return setTaskAsInterrupted(taskId)().then(
                    prevComponent.cancel.bind(prevComponent)()
                ).then(() => resolve())
            },  
            prevComponent,  // FOR SAFETY, THIS IS ALSO INHERITED
        );
    });
    const onCommandTask: TaskAction = task => new Promise(resolve => {
        // TODO: IMPLEMENT SSH COMMAND EXECUTION AND LOGGING
        logger.error("-------------------------------------------");
        logger.error("|SSH Command execution is not implemented.|");
        logger.error("-------------------------------------------");
        resolve();
    });
    const onEndlessTask: TaskAction = task => new Promise(resolve => {
        const prevComponent = TaskComponent.componentMap[task._id];
        return TaskComponent.componentMap[task._id].isTaskFinished() ? 
        resolve() :
        TaskComponent.componentMap[task._id] = new TaskComponent(
            prevComponent,
            prevComponent,
            () => {
                // CANCEL IS NOW KILLING
                return setTaskAsInterrupted(taskId)().then(
                    prevComponent.cancel.bind(prevComponent)
                ).then(() => resolve());
            },
            prevComponent
        )
    });

    logger.debug(`Executing the Task#${ taskId }...`);
    return taskRepository.findById(taskId).then(
        populateTask
    ).then((task): [PopulatedTask, TaskAction] => [
        /* ---- DECIDING ABOUT THE NEXT ACTION ---- */
        task, 
        task.template.command ? onCommandTask : // CASE HAS COMMAND
        task.endDate ? onTimerTask : // CASE HAS END DATE
        onEndlessTask // ELSE
    ]).then(([pTask, action]) => action(pTask)).then(() => {
        TaskComponent.componentMap[taskId].complete();
    }).then(() => {
        logger.debug(`...Done executing the Task#${ taskId }`);
    }).catch((err: Error) => {
        logger.error("Unexpected bahavior inside the onTaskExecute handler");
        logger.error(err.message);
        logger.error(err.stack);
        process.exit(-1);
    });
}